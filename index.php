<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Asiakkaat</title>
        <link href="css/style.css" type="text/css" rel="stylesheet">
    </head>
    <body>
        <h3>Asiakkaat</h3>
        <a href="asiakas.php">Lisää uusi</a>
        <?php
        try{
            //avataan tietokantayhteys
            $tietokanta = new PDO('mysql:host=localhost;dbname=asiakasrekisteri;charset=utf8','root','');
            //Oletuksena PDO ei näytä mahdollisia virheitä, joten asetetaan "virhemoodi" päälle.
            $tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            //Muodostetaan suoritettava sql-lause
            $sql = 'SELECT * FROM asiakas ORDER BY sukunimi';
            
            //Suoritetaan kysely tietokantaan
            $kysely = $tietokanta->query($sql);

            if ($kysely) {

                //tulostetaan tietokannasta haetut tiedot selaimeen HTML-taulukkoon
                print '<table>';
                print '<tr>';
                print '<th>Sukunimi</th>';
                print '<th>Etunimi</th>';
                print '<th>Email</th>';
                print '<th></th>';
                print '<th></th>';
                print '<th></th>';
                print '</tr>';

                while ($tietue = $kysely->fetch()) {
                    print '<tr>';
                    print '<td>' . $tietue['sukunimi'] . '</td>';
                    print '<td>' . $tietue['etunimi'] . '</td>';
                    print '<td>' . $tietue['email'] . '</td>';
                    print '<td><a href="asiakas.php?id=' . $tietue['id'] . '">Muokkaa</a></td>';
                    print '<td><a href="poista.php?id=' . $tietue['id'] . '" onclick="return confirm(\'Jatketaanko?\');">Poista</a></td>';
                    print '<td><a href="muistiinpanot.php?id' .$tietue['id'] . '">Muistiinpanot</a></td>';
                    print '</tr>';
                }

                print '</table>';
            }
            else {
                print '<p>';
                print_r($tietokanta->errorInfo());
                print '</p>';
            }
        } catch (PDOException $pdoex) {
            print '<p>Tietokannan avaus epäonnistui ' . $pdoex->getMessage() .'</p>';
        }
        ?>
    </body>
</html>
