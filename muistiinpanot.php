<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Lisää muistiinpano</title>
    </head>
    <body>
        <h3>Muistiinpanot</h3>
        <?php
        $id = 0;
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $id = filter_input(INPUT_GET,'id',FILTER_SANITIZE_NUMBER_INT);
        }
        ?>
        
        <form action="<?php print($_SERVER['PHP_SELF']);?>" method="post">
            <input type="hidden" name="id">
            <br>
            <textarea name="text" rows="4" cols="50"></textarea>
            <br>
            <input type="submit" value="Lisää">
        </form>
        
        <?php
        try {
            $tietokanta = new PDO('mysql:host=localhost;dbname=asiakasrekisteri;charset=utf8','root','');
            
            $tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            $sql = "SELECT * FROM muistiinpano WHERE asiakas_id=$id ORDER BY aika DESC";
            
            $kysely = $tietokanta->query($sql);
            
            print '<div>';
            
            while ($tietue = $kysely->fetch()) {
                print '<p>';
                print '<span>' . date('d.m.Y H.i',strtotime($tietue['aika'])) . '</span><br />';
                print $tietue['teksti'];
                print '<p>';
            }
        } catch (Exception $pdoex) {
            print '<p>Häiriö tietokannassa.' . $pdoex->getMessage(). '</p>';
        }
        ?>
    </body>
</html>
