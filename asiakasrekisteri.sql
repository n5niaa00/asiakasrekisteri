drop database if exists asiakasrekisteri;

create database asiakasrekisteri;

use asiakasrekisteri;

create table asiakas(
  	id int primary key auto_increment,
    etunimi varchar(50) not null,
    sukunimi varchar(50) not null,
    email varchar(255)
);

create table muistiinpano(
    id int primary key auto_increment,
    teksti text not null,
    asiakas_id int not null,
    foreign key (asiakas_id) references asiakas(id)
    on delete restrict,
    aika timestamp default current_timestamp
    on update current_timestamp
);
insert into asiakas(etunimi, sukunimi, email) values('Jouni','Juntunen','joini.juntunen@oamk.fi');
insert into muistiinpano (teksti,asiakas_id) values('Testiä',1);